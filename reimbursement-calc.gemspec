Gem::Specification.new do |s|
    s.name        = 'reimbursement-calc'
    s.version     = '0.0.1'
    s.summary     = "A reimbursement calculator test"
    s.description = "A simple test using a reimbursement scenario."
    s.authors     = ["Justin Kauszler"]
    s.email       = 'jkauszler@gmail.com'
    s.files       = ["lib/reimbursement-calc.rb"]
    s.license     = 'MIT'
  end
  